// EX1: Tính Tiền Lương Nhân Viên
document.getElementById("btnSalary").onclick = function() {
    var num = document.getElementById("inputNum1").value * document.getElementById("inputNum2").value;
    document.getElementById("txtSalary").innerHTML = num;
}
// EX2: Tính Trung Bình
document.getElementById("btnTB").onclick = function () {
    let num = document.querySelectorAll(".ex2 .form-control") 
    , t = 0;
    for (let n = 0 ; n < num.length; n++) 
    t += Number(num[n].value);
    document.getElementById("txtTB").innerHTML = t / num.length     
    }
    // EX3 ; Quy Đổi USD
    document.getElementById("btnCurrency").onclick = function() {
        var num = document.getElementById("usd").value
          , t = new Intl.NumberFormat("vn-VN").format(23500 * num);
        document.getElementById("txtCurrency").innerHTML = t
    }
    // EX4 : Tính Chu Vi , Diện Tích HCN
    document.getElementById("btnCal").onclick = function() {
    var num = document.getElementById("width").value
      , t = document.getElementById("height").value
      , n = num * t
      , u = 2 * (Number(num) + Number(t));
    document.getElementById("txtCal").innerHTML = `\n        Diện tích: ${n};\n        Chu vi: ${u}\n    `
}
// EX5 : Tính Tổng 2 Ký Số
document.getElementById("btnSum").onclick = function() {
    var num = document.getElementById("number").value
      , t = Math.floor(num / 10)
      , n = num % 10;
    document.getElementById("txtSum").innerHTML = t + n
}